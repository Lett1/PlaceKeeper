﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaceKeeper
{
    class Place : INotifyPropertyChanged
    {
        private string name;
        private string location;
        private int population;

        public event PropertyChangedEventHandler PropertyChanged;

        public int Population
        {
            get => population;
            set
            {
                population = value;
                OnPropertyChanged("Population");
            }
        }
        public string Location
        {
            get => location;
            set
            {
                location = value;
                OnPropertyChanged("Location");
            }
        }

        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public Place(string name, string location, int population)
        {
            this.Name = name;
            this.Location = location;
            this.Population = population;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
