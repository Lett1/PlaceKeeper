﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlaceKeeper
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var list = (IList<Place>)LocationList.ItemsSource;
            list.Add(new Place("Durr", "hurr", 2000));
            
        }

        private void AddPlace_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var list = (IList<Place>)LocationList.ItemsSource;

            list.Add(new Place("Durr", "Nowhere", 1337));
        }

        private void RemovePlace_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = LocationList.SelectedIndex > -1;
        }

        private void RemovePlace_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var list = (IList<Place>)LocationList.ItemsSource;

            list.RemoveAt(LocationList.SelectedIndex);
        }

        private void FilterButton_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var button = (Button)e.OriginalSource;
            button.ContextMenu.IsEnabled = true;
            button.ContextMenu.PlacementTarget = button;
            button.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            button.ContextMenu.IsOpen = true;
        }
    }
}
